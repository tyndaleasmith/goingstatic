# Going Static in a Dynamic World

This website is an example website and presentation for the CCCU 2016 Commission on Technology Conference » <http://www.cccu.org/ConferencesAndEvents/CalendarOfEvents/2016/6/2016_COT>

## Session Desecription

The web development world is full of great dynamic content management systems like Drupal, Wordpress and SharePoint and Tyndale has used all of these for specific use cases. But they all come with ongoing maintenance costs. I will share how we have used static website generators (Jekyll) to speed up website development and cut hosting and ongoing maintenance cost.
