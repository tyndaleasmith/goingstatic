---
layout:     slide
# Slide metadata
slideid:	demo
slide-img: 	
# display-text - use 1 for yes, leave blank for no --> no = only the background image
display-text: 1
title:      "What do you mean by \"Static\"?"
subtitle:	"stat·ic &ndash; lacking in movement, action, or change, especially in a way viewed as undesirable or uninteresting."
lead:		"This is a short paragraph that will encompass the main point of the slide. This will appear below the slide and help describe the point in more detail. This is optional."
next-link:	/start/ 
# URL metatdata
permalink: 	/demo/
---


## » Website Overview

A quick look through the structure of the Tyndale website looking at editable regions, menus and URL patterns.

---

## » Understanding Content Types

The Tyndale website contains many types of content, such as **basic pages, courses, events, persons, programs, webforms** etc. Each one of these content types have been described inside of Drupal and will allow you to insert the pieces required for that specific type of content.

Some content types are designed to **store specific information** (ex. courses, events, persons) while others are designed to **provide specific functionality** with some flexibility in the content (ex. basic page, webform, audio page.)

---

## » Understanding Menus
**Menu Types:**

* **Global Navigation** (same across the entire website)
* **Local Navigation** (local to the context of the content)

**Parent/Child Menu Item Placement**    
\<Menu Title\>    
– Menu Item Title     
— Child Menu Item Title   

**Menu Weights** Position/order on the menu

The lower the number, the higher on the menu. The higher the number, the lower on the menu. Lower number have less weight and float to the top of the menu higher numbers have more weight and sink to the bottom of the menu.

**Note:** The Webteam can visually reorder your menu for you if you are having difficulty getting items into the right position.

