---
layout:     	slide
# Slide metadata
slideid:		notext
slide-img: 		"img/code-ish.png"
# display-text - use 1 for yes, leave blank for no --> no = only the background image 
display-text: 	
title:      	"Slide Without a Visible Title"
subtitle:		"yup"
lead:			"This is a short paragraph that will encompass the main point of the slide. This will appear below the slide and help describe the point in more detail. This is optional."
next-link:		/demo/ 
# URL metatdata
permalink: 		/notext/
---