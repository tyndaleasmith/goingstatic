---
layout: slide
#
# Slide metadata
slideid: jekyll-why
slide-img:
#
# display-text - use 1 for yes, leave blank for no --> no = only the background image
display-text: 1
title: "Why We Chose Jekyll"
subtitle: "There are many great choices..."
lead: "There are <a href=\"http://www.staticgen.com\">lots of static website generators</a> — we choose to use <a href=\"http://jekyllrb.com/\">Jekyll</a> and this is why: <small class=\"fa-red\">*</small>"
next-link: /jekyll-how/ 
#
# URL metatdata
permalink: /jekyll-why/
---

1. **Jekyll is very popular!** It is a very mature product, is the top-rated static site generator on [Github][gh] and has a huge community!
2. **Jekyll is simple to use!** It is easy to get started with creating basic websites.
3. **Jekyll is powerful!** You can create very robust websites.
4. **Jekyll is very well documented!** The official [documentation][docs] is great and there are many other great [resources][resources] around the web.
5. **Jekyll is Open Source!** Open Source is great for budget sensitive institutions. Jekyll is released under the [MIT License][osl].
6. **Jekyll does what you tell it to do** — no more, no less. It doesn’t try to outsmart users by making bold assumptions, nor does it burden them with needless complexity and configuration.
7. **Jekyll has many deployment methods** available and some are even free! Whether you want to FTP to an internal server, host on a cheap VPS or deploy using the free [Github Pages][gp] service — your options don't require a robust server infrastructure!
8. **Jekyll didn't require learning a new language.** Jekyll is built with Ruby, but you don't have to know Ruby to use it. 

<small class="fa-red">* These reasons are not listed in order of importance.</small>





[jk]: http://jekyllrb.com
[gh]: https://github.com/jekyll/jekyll
[docs]: http://jekyllrb.com/docs/home 
[resources]: /resources/
[gp]: https://pages.github.com
[osl]: https://github.com/jekyll/jekyll/blob/master/LICENSE