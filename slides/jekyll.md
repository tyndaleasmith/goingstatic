---
layout: slide
#
# Slide metadata
slideid: jekyll
slide-img: "img/gs-jekyll.jpg"
#
# display-text - use 1 for yes, leave blank for no --> no = only the background image
display-text: 
title: "Introducing Jekyll"
subtitle: "– a simple, blog-aware, static site generator"
lead: "<a href=\"http://jekyllrb.com/\">Jekyll</a> is a simple, blog-aware, static site generator. It takes a template directory containing raw text files in various formats, runs it through a converter (like Markdown) and our Liquid renderer, and spits out a complete, ready-to-publish static website suitable for serving with your favorite web server."
next-link: /jekyll-why/ 
#
# URL metatdata
permalink: /jekyll/
---