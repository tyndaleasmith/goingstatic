---
layout: slide
#
# Slide metadata
slideid: questions
slide-img:
#
# display-text - use 1 for yes, leave blank for no --> no = only the background image
display-text: 1
title: "Questions?"
subtitle: "– and possibly some answers"
lead: 
next-link: /thanks/ 
#
# URL metatdata
permalink: /questions/
---