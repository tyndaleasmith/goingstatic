---
layout: slide
#
# Slide metadata
slideid: our-problem
slide-img: "img/gs-lab.jpg"
#
# display-text - use 1 for yes, leave blank for no --> no = only the background image
display-text: 1
title: "What was the Core Problem we were Trying to Solve?"
subtitle:
lead: "As we build more and more websites, how do we manage the cost of development and ongoing maintenance?"
next-link: /going-static/ 
#
# URL metatdata
permalink: /our-problem/
---