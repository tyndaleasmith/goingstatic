---
layout: slide
#
# Slide metadata
slideid: start
slide-img: "img/gs-blank.jpg"
#
# display-text - use 1 for yes, leave blank for no --> no = only the background image
display-text: 1
title: "Going Static in a Dynamic World"
subtitle: "2016 Commission on Technology Conference"
lead: "The web development world is full of great dynamic content management systems like Drupal, Wordpress and SharePoint and Tyndale has used all of these for specific use cases. But they all come with ongoing maintenance costs. I will share how we have used static website generators (Jekyll) to speed up website development and cut hosting and ongoing maintenance cost."
next-link:		/intro/ 
#
# URL metatdata
permalink: 		/start/
---