---
layout: slide
#
# Slide metadata
slideid: complexity
slide-img: "img/gs-complexity3.jpg"
#
# display-text - use 1 for yes, leave blank for no --> no = only the background image
display-text: 1
title: "Complexity of the Web in 2016"
subtitle:
lead: "Our first website (1996) contained approximately 20 pages and used one programming language (HTML)"
next-link: /complexity-burden/ 
#
# URL metatdata
permalink: /complexity/
---

Tyndale first published a website in April of 1996 ([wayback][wb]). We had humble beginnings with a website that contained approximately 20 web pages and used one programming language (HTML). Websites were generally viewed on one, maybe two screen sizes and the outcome was predictable.

Currently, we manage multiple websites with over 20,000 unique pages or URLs. We utilize multiple content management systems (CMS) and use multiple program languages (HTML, CSS, JavaScript, SASS, PHP and Ruby) to build and deploy these sites. We manage the source code of these projects in a complex version control system (GIT) and deploy the code to multiple server locations using both GIT and SFTP.

The trend of people viewing websites on small-screen mobile devices is in rapid growth. At the same time, large high definition screens are becoming the norm on desktop and laptop computers. Internet bandwidth is continually getting faster, and people are expecting bandwidth intense content such as video and high resolution photos to work flawlessly, no matter what device they are viewing the website on.

With the growing number of browser and operating system combinations coupled with device type and screen resolution combinations, testing on each of these combinations is basically impossible, possibly inconceivable, certainly improbable.

We are also challenged with building and testing websites that work effectively using keyboard and mouse input, and equally effective using a finger on a touch screen. We need to balance these goals with building a website that adheres to strict web content accessibility guidelines (WCAG).

…oh, and don’t forget to make it look pretty…

[wb]: https://web.archive.org/web/19970601055908/http://www.obcots.on.ca/