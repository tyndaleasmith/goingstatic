---
layout: slide
#
# Slide metadata
slideid: resources
slide-img:
#
# display-text - use 1 for yes, leave blank for no --> no = only the background image
display-text: 1
title: "Resources"
subtitle: "Documentation / Articles / Videos"
lead: "Looking for a place to start your research? Here are some resources for static site generators and Jekyll."
next-link: /source-code/ 
#
# URL metatdata
permalink: /resources/
---

## <i class="fa fa-puzzle-piece">&nbsp;</i>Static Site Generators

* [StaticGen][static] — Top Open-Source Static Site Generators using varied technologies
* [{static is} The New Dynamic ][new-dynamic] — The Static Web: Static Site Generators and the Post-CMS paradigm.
* [An Introduction to Static Site Generators][intro-to-ssg] — By [Eduardo Bouças](http://eduardoboucas.com/) on May 20, 2015  

---

## <i class="fa fa-list-ul">&nbsp;</i>Jekyll Resources

* [Jekyll website][jk]
* [Jekyll source code on Github][gh]
* [Official Jekyll Documentation][docs]
* [Jekyll Tips][tips]
    * [Jekyll Casts][tips] (video tutorials <i class="fa fa-thumbs-o-up">&nbsp;</i>)
    * [Get Started Guide][started]
    * [Jekyll Cheat Sheet][cheat]
* [Jekyll Themes][themes]
* [Awesome Jekyll][awesome-jekyll] (a collection of awesome Jekyll goodies)
* [Liquid Template language][liquid]
    * [Liquid for Programmers][lp] 
    * [Liquid for Designers][ld]
    * [Liquid screencast][ls]

## <i class="fa fa-edit">&nbsp;</i>Jekyll Articles

- [Achieving 100 / 100 on Google PageSpeed Insights (using Jekyll)](https://www.smartiehastheanswer.co.uk/achieving-100-out-of-100-on-google-pagespeed-insights-using-jekyll.html) by Andy Smart; March 2016
- [Goodbye Medium, Hello Jekyll](http://intersect.whitefusion.io/open-web/goodbye-medium-hello-jekyll) by Jared White; March 2016
- [How I'm Using Jekyll in 2016](https://mademistakes.com/articles/using-jekyll-2016) by Michael Rose; 2016
- [How to Create an Open Source Directory on GitHub Pages](http://webdesign.tutsplus.com/tutorials/how-to-create-an-open-source-directory-on-github-pages--cms-26225) by David Darnes; March 2016
- [Why I left Blogger for Jekyll](https://era86.github.io/2016/03/26/why-i-left-blogger-for-jekyll.html) by Fred; March 2016
- [Consider Jekyll: why I use Jekyll to publish on the web and why you might be interested](https://olivermak.es/2016/03/consider-jekyll) by Oliver Pattison; March 2016
- see [more at Awesome Jekyll][awesome-jekyll]...




[static]: http://www.staticgen.com/
[new-dynamic]: https://www.thenewdynamic.org
[intro-to-ssg]: https://davidwalsh.name/introduction-static-site-generators
[jk]: http://jekyllrb.com
[gh]: https://github.com/jekyll/jekyll
[docs]: http://jekyllrb.com/docs/home 
[tips]: http://jekyll.tips/
[started]: http://jekyll.tips/guide/getting-started-with-jekyll/
[cheat]: http://cheat.jekyll.tips/
[themes]: http://jekyllthemes.org/
[awesome-jekyll]: https://github.com/planetjekyll/awesome-jekyll
[liquid]: https://github.com/Shopify/liquid/wiki
[lp]: https://github.com/Shopify/liquid/wiki/Liquid-for-Programmers
[ld]: https://github.com/Shopify/liquid/wiki/Liquid-for-Designers
[ls]: http://railscasts.com/episodes/118-liquid

