---
layout: slide
#
# Slide metadata
slideid: source-code
slide-img:
#
# display-text - use 1 for yes, leave blank for no --> no = only the background image
display-text: 1
title: "Get the Source Code"
subtitle: "Download this presentation website on Bitbucket"
lead: "This presentation is a Jekyll website! You can <a href=\"https://bitbucket.org/tyndaleasmith/goingstatic\" target=\"_blank\">download the source code for this presentation on Bitbucket</a>."
next-link: /questions/ 
#
# URL metatdata
permalink: /source-code/
---


<i class="fa fa-chain fa-red">&nbsp;</i>Download Source Code: <https://bitbucket.org/tyndaleasmith/goingstatic> 

<i class="fa fa-chain fa-red">&nbsp;</i>View Website: <http://gs.tyndale.ws> 
