---
layout: slide
#
# Slide metadata
slideid: complexity-burden
slide-img: "img/gs-construction.jpg"
#
# display-text - use 1 for yes, leave blank for no --> no = only the background image
display-text: 1
title: "Complexity Brings a Burden of Maintenance"
subtitle:
lead: "These complexities not only make the craft of building websites more challenging, they also make the maintenance of the websites an ongoing challenge."
next-link: /our-problem/ 
#
# URL metatdata
permalink: /complexity-burden/
---

Modern, database-driven content management systems are built on complex technology stacks, store data in venerable databases, and are written in programming languages that can expose security issues. The CMS vendors are continually monitoring security issues and updating the software to counteract those issues.

It then becomes our burden to monitor these security updates and deploy the updates on each of the websites that use that CMS. These changes happen at least once a month but often happen on a more regular basis. The cost of ignoring these updates can be substantial, even a complete loss of the site and its content.