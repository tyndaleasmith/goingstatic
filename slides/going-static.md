---
layout: slide
#
# Slide metadata
slideid: going-static
slide-img: "img/gs-light-bulb.jpg"
#
# display-text - use 1 for yes, leave blank for no --> no = only the background image
display-text: 1
title: "Going Static"
subtitle: "A simple solution to a complex problem?"
lead: "Find strategic places to replace dynamic CMS driven websites with simplified static websites."
next-link: /jekyll/ 
#
# URL metatdata
permalink: /going-static/
---

Asking questions like: 

* How often does this website need to be updated?
* Do we need to provide access to staff to update this website?
* Is there any functionality that requires a database?