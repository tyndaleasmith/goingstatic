---
layout: slide
#
# Slide metadata
slideid: dynamic-components
slide-img:
#
# display-text - use 1 for yes, leave blank for no --> no = only the background image
display-text: 1
title: "Adding Dynamic Components"
subtitle: "Static Meets Dynamic"
lead: "Whether you want to add comments to your blog posts, a webform on your contact page or photos from your Flickr stream, Jekyll can handle it!"
next-link: /use-cases/ 
#
# URL metatdata
permalink: /dynamic-components/
---

For a great list of third-party service options check out <http://jekyll.tips/services>