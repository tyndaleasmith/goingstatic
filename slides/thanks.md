---
layout: slide
#
# Slide metadata
slideid: thanks
slide-img:
#
# display-text - use 1 for yes, leave blank for no --> no = only the background image
display-text: 1
title: "Thanks!"
subtitle: "Follow me on Twitter — @youthpoint"
lead: 
next-link:  
#
# URL metatdata
permalink: /thanks/
---