---
layout: slide
#
# Slide metadata
slideid: intro
slide-img: "img/gs-hello.jpg"
#
# display-text - use 1 for yes, leave blank for no --> no = only the background image
display-text: 
title: "Hello my name is Andy Smith"
subtitle:
lead: "Web Developer with 20 years experience in the Christian Higher Education sector. I've seen it all..."
next-link: /our-core/ 
#
# URL metatdata
permalink: /intro/
---

## About Me

Hi! I'm Andy, I currently work as the Website Administrator & Lead Developer at [Tyndale University College & Seminary][tyndale]. I have been building websites professionally in the Christian Higher Education sector for 20 years. I love spending quality time in my [hammock][hammocks]!

---

## About Tyndale

[Tyndale][tyndale] is a multidenominational Christian university college and seminary, legislated by the province of Ontario. The new 56-acre campus is strategically positioned in north Toronto on Bayview Avenue. Tyndale’s diverse student body is engaged in comprehensive post-secondary education programs taught from a faith perspective. [Tyndale University College][uc] provides undergraduate degree programs that prepare students for professional vocations and postgraduate studies. [Tyndale Seminary][sem] provides postgraduate theological education at the master’s and doctoral levels for Christian leaders in the 21st century. Founded in 1894, Tyndale presently has over 1,600 students representing over 40 denominations and 60 ethnic backgrounds and more than 12,500 alumni. 


[tyndale]: http://www.tyndale.ca
[uc]: http://www.tyndaleu.ca
[sem]: http://www.tyndale.ca/seminary
[hammocks]: http://hammock.tyndale.ws