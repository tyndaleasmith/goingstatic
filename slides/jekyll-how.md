---
layout: slide
#
# Slide metadata
slideid: jekyll-how
slide-img:
#
# display-text - use 1 for yes, leave blank for no --> no = only the background image
display-text: 1
title: "How does Jekyll Work?"
subtitle: "Here's the basic ideas..."
lead: "Jekyll runs a Ruby Gem on your computer and comes with a built-in development server that will allow you to preview what the generated site will look like in your browser locally. Once <a href=\"https://jekyllrb.com/docs/installation\">installed</a>, you can and use the command line to get a new website started in seconds..."
next-link: /dynamic-components/ 
#
# URL metatdata
permalink: /jekyll-how/
---


{% highlight bash linenos %}
~$ gem install jekyll
~$ jekyll new my-awesome-site
~$ cd my-awesome-site
~$ jekyll serve
# => Now browse to http://localhost:4000   
{% endhighlight %}
<small><i class="fa fa-chevron-up">&nbsp;</i><https://jekyllrb.com/></small>

---

**<i class="fa fa-paperclip">&nbsp;</i>Note:** This is a brief overview – please check out [the Resources slide](/resources/) to get links to more information.

---

### <i class="fa fa-folder-open">&nbsp;</i>Directory Structure

<i class="fa fa-sitemap">&nbsp;</i>A basic Jekyll site usually looks something like this:

{% highlight bash %}
.
├── _config.yml
├── _drafts
|   ├── begin-with-the-crazy-ideas.textile
|   └── on-simplicity-in-technology.markdown
├── _includes
|   ├── footer.html
|   └── header.html
├── _layouts
|   ├── default.html
|   └── post.html
├── _posts
|   ├── 2007-10-29-why-every-programmer-should-play-nethack.textile
|   └── 2009-04-26-barcamp-boston-4-roundup.textile
├── _data
|   └── members.yml
├── _site
├── .jekyll-metadata
└── index.html
{% endhighlight %}

<small><i class="fa fa-chevron-up">&nbsp;</i><https://jekyllrb.com/docs/structure></small>

---


### <i class="fa fa-code">&nbsp;</i>_config.yml, _includes, _layouts, _posts, _data and _site

* **_config.yml** – stores global configuration data. 
* **_includes** – These are the partials that can be mixed and matched by your layouts and posts to facilitate reuse.
* **_layouts** – These are the templates that wrap posts. Layouts are chosen on a post-by-post basis in the YAML Front Matter.
* **_posts**  – contains the blog posts for your website
* **_data** - Data files allow you to access information from CSV, JSON or YAML files on your Jekyll website. 
* **_site** — This is where the generated site will be placed (by default) once Jekyll is done transforming it. 
* **Other** — Every other directory and file (such as css and images folders, favicon.ico files) will be copied verbatim to the generated site. 

» Get a [detailed description][structure] of these special files and directories in the [documentation][structure]


---

### <i class="fa fa-desktop">&nbsp;</i>Running Jekyll — Develop Locally

Jekyll comes with a built-in development server that will allow you to preview what the generated site will look like in your browser locally.


{% highlight bash linenos %}
~$ jekyll serve
# => A development server will run at http://localhost:4000/
# Auto-regeneration: enabled. Use `--no-watch` to disable.
{% endhighlight %}

---

### <i class="fa fa-files-o">&nbsp;</i>Creating Pages and Posts

Create pages and posts using:

* HTML
* [Markdown][md]
* [Textile][tt] (with plugins)
* [Other formats][other] (with plugins)

---

### <i class="fa fa-code">&nbsp;</i>Front Matter (YAML)

You can use YAML Front Matter to set variables on your posts or pages.

Example YAML Front Matter —
{% highlight yaml linenos %}
---
layout: post
title:  Weekly Meeting Agenda
date:  2016-05-18 13:00:00
categories: agenda
---
{% endhighlight %}

Use these variables to:

* Select the layout
* Set the title of the page or post
* Set the date of the post
* Put post into categories
* and more...

» More information in the [Front Matter documentation][fm]

---

### <i class="fa fa-wrench">&nbsp;</i>Content Modeling with YAML Front Matter

Create custom content models with a combination of YAML front matter and a custom layout file (ex. /_layouts/slide.html)



Example YAML Front Matter —
{% highlight yaml linenos %}
---
layout: slide
#
# Slide metadata
slideid: sample-slide
slide-img:
#
# display-text - use 1 for yes, leave blank for no --> no = only the background image
display-text: 1
title: "Slide Title Text"
subtitle: "Slide Subtitle Text"
lead: "This is a short paragraph that will encompass the main point of the slide. This will appear below the slide and help describe the point in more detail."
next-link: /next-url/ 
#
# URL metatdata
permalink: /sample-slide/
---
{% endhighlight %}


---

### <i class="fa fa-object-group">&nbsp;</i>Layouts and Liquid

Jekyll uses the [Liquid][liquid] templating language to process templates. All of the standard Liquid tags and filters are supported. Jekyll even adds a few handy filters and tags of its own to make common tasks easier.

» More information in the [Templates documentation][templates]

---

### <i class="fa fa-globe">&nbsp;</i>Posting to the Web

Sites built using Jekyll can be deployed in a large number of ways due to the static nature of the generated output.

* Web hosting providers (FTP)
* Self-managed web server (FTP, SCP, or even direct filesystem access)
* rsync
* Amazon S3
* [Github Pages][ghp]
* [CloudCannon][cc] (Jekyll + CMS)

---

### <i class="fa fa-edit">&nbsp;</i>The Rest of the [Documentation][docs]...

Read to [official Jekyll documentation][docs] or checkout the [Resources slide](/resources/) for additional information.


[docs]: https://jekyllrb.com/docs/home
[structure]: https://jekyllrb.com/docs/structure
[md]: https://daringfireball.net/projects/markdown/
[tt]: http://redcloth.org/textile
[other]: https://jekyllrb.com/docs/plugins/#converters-1
[ghp]: https://pages.github.com/
[cc]: http://cloudcannon.com/
[fm]: https://jekyllrb.com/docs/frontmatter/
[liquid]: https://github.com/Shopify/liquid/wiki
[templates]: https://jekyllrb.com/docs/templates/
