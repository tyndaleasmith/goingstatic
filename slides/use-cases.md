---
layout: slide
#
# Slide metadata
slideid: use-cases
slide-img:
#
# display-text - use 1 for yes, leave blank for no --> no = only the background image
display-text: 1
title: "Use Case Examples"
subtitle: "Some of the ways we have used static websites at Tyndale"
lead: "You can use Jekyll to build anything from simple websites to very complex websites. Here are some of the ways I have used Jekyll:"
next-link: /resources/ 
#
# URL metatdata
permalink: /use-cases/
---

* [Tyndale Annual Impact Report][impact]
* [Tyndale Update][update]
* [Capital Campaign Website][ml] (not Tyndale)
* [Blog][hammock] (Hammockable)
* [Pattern Library][pl] (a work in progress)
* [Documentation][doc] (a work in progress)
* [URL Redirect service][go] (using [pageless_redirects.rb][pr])
* [Presentations][gs] (This website is made with Jekyll)


[impact]: http://www.tyndaleimpact.ca
[update]: http://www.tyndaleupdate.ca
[pl]: http://pl.tyndale.ws 
[hammock]: http://hammock.tyndale.ws
[doc]: http://webhelp.tyndale.ws
[ml]: http://medebamountainlodge.com
[gs]: http://gs.tyndale.ws
[go]: http://go.tyndale.ca/
[pr]: https://github.com/nquinlan/jekyll-pageless-redirects