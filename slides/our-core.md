---
layout: slide
#
# Slide metadata
slideid: our-core
slide-img: "img/gs-tyndale.jpg"
#
# display-text - use 1 for yes, leave blank for no --> no = only the background image
display-text: 1
title: "Our Core Website Infrastructure at Tyndale"
subtitle: "The main tools we use"
lead: "We love using <a href=\"http://drupal.org\">Drupal</a> but use other tools when needed!"
next-link: /complexity/ 
#
# URL metatdata
permalink: /our-core/
---

# How we build websites at Tyndale

The main Content Management System (CMS) we use at Tyndale is **[Drupal][drupal]** — an open source CMS build using PHP. We host our core websites on **[Pantheon][pantheon]** and our less important Drupal sites on a **[Dreamhost VPS][dh]**.

For our [blogging][blog] needs we also use a multi-site setup of **[Wordpress][wp]** hosted on the **[Dreamhost VPS][dh]**.

[drupal]: http://drupal.org
[pantheon]: http://pantheon.io
[dh]: https://www.dreamhost.com/r.cgi?260229
[wp]: http://www.wordpress.org
[blog]: http://tyndaleblogs.ca/