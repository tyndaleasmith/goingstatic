---
layout: md-page
title: "Index of all of the slides"
pageid: slide-index
description:  
header-img: 
permalink: /slide-index/
---

# <i class="fa fa-list-ul">&nbsp;</i>Presentation Slide Index

1. [Starting Slide / Session Description](/start/)
1. [Intro/About](/intro/)
1. [Our core website infrustructure](/our-core/)
1. [Complexity of the web in 2016](/complexity/)
1. [Complexity brings a burden of maintenance](/complexity-burden/)
1. [The core problem we were trying to solve](/our-problem/)
1. [Going Static](/going-static/)
1. [Introducing Jekyll](/jekyll/)
1. [Why Jekyll?](/jekyll-why/)
1. [How does Jekyll work?](/jekyll-how/)
1. [Adding dynamic components](/dynamic-components/)
1. [Use case examples](/use-cases/)
1. [Resourses](/resources/)
1. [Get the source code](/source-code/)
1. [Questions/Answers](/questions/)
1. [Thanks and contact info](/thanks/)