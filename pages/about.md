---
layout: md-page
title: "A little bit more about..."
pageid: about
description:  
header-img: "img/stained-glass-tyndale.jpg"
permalink: /about/
---

## About this Session

The web development world is full of great dynamic content management systems like Drupal, Wordpress and SharePoint and Tyndale has used all of these for specific use cases. But they all come with ongoing maintenance costs. I will share how we have used static website generators (Jekyll) to speed up website development and cut hosting and ongoing maintenance cost.

**— Thursday June 9, 2016 at 10:30am**

---

## About the Presenter

Andy Smith works as the Website Administrator & Lead Developer at [Tyndale University College & Seminary][tyndale]. He has been building websites professionally in the Christian Higher Education sector for 20 years. Andy and his wife Sandra live in Bradford Ontario (that's in Canada) and have two children, Rachael (20) and Josh (17).

---

## About Jekyll

[Jekyll][jekyll] is a simple, blog-aware, static site generator. It takes a template directory containing raw text files in various formats, runs it through a converter (like [Markdown][md]) and our [Liquid][liquid] renderer, and spits out a complete, ready-to-publish static website suitable for serving with your favorite web server. <small>([source][about-jekyll])</small>

---

## About Tyndale

[Tyndale][tyndale] is a multidenominational Christian university college and seminary, legislated by the province of Ontario. The new 56-acre campus is strategically positioned in north Toronto on Bayview Avenue. Tyndale’s diverse student body is engaged in comprehensive post-secondary education programs taught from a faith perspective. [Tyndale University College][uc] provides undergraduate degree programs that prepare students for professional vocations and postgraduate studies. [Tyndale Seminary][sem] provides postgraduate theological education at the master’s and doctoral levels for Christian leaders in the 21st century. Founded in 1894, Tyndale presently has over 1,600 students representing over 40 denominations and 60 ethnic backgrounds and more than 12,500 alumni.

---

## About My Hammocks

Tyndale has a beautiful new campus which has some amazing outdoor space. I have been enjoying finding great places to hang my hammock. [I have recorded these hammockable spots online][hammocks] for you... <small>(made with [Jekyll][jekyll])</small>


[jekyll]: http://jekyllrb.com/
[md]: https://daringfireball.net/projects/markdown/
[liquid]: https://github.com/Shopify/liquid/wiki
[about-jekyll]: https://jekyllrb.com/docs/home/
[tyndale]: http://www.tyndale.ca
[uc]: http://www.tyndaleu.ca
[sem]: http://www.tyndale.ca/seminary
[hammocks]: http://hammock.tyndale.ws

